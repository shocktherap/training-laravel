<?php

namespace App;

class Numerica
{
  private $list = array();

  public function __construct($list)
  {
    $this->list = $list;
  }

  function first(){
    return \reset($this->list);
  }

  function last(){
    return \end($this->list);
  }

  function min(){
    return \min($this->list);
  }

  function max(){
    return \max($this->list);
  }

  function sum(){
    return \array_sum($this->list);
  }

  function sort(){
    \sort($this->list);
    return $this->list;
  }

  function rsort(){
    \rsort($this->list);
    return $this->list;
  }

  function odd(){
    $oddList = [];
    foreach ($this->list as $number) {
      if($number % 2 != 0){
        array_push($oddList, $number);
      }
    }
    return $oddList;
  }

  function even(){
    $evenList = [];
    foreach ($this->list as $number) {
      if($number % 2 == 0){
        array_push($evenList, $number);
      }
    }
    return $evenList;
  }

  function greaterThan($number){
    $greaterList = [];
    foreach ($this->list as $var) {
      if($var > $number){
        array_push($greaterList, $var);
      }
    }
    return $greaterList;
  }

  function greaterThanEqual($number){
    $greaterList = [];
    foreach ($this->list as $var) {
      if($var >= $number){
        array_push($greaterList, $var);
      }
    }
    return $greaterList;
  }

  function lessThan($number){
    $lessList = [];
    foreach ($this->list as $var) {
      if($var < $number){
        array_push($lessList, $var);
      }
    }
    return $lessList;
  }

  function lessThanEqual($number){
    $lessList = [];
    foreach ($this->list as $var) {
      if($var <= $number){
        array_push($lessList, $var);
      }
    }
    return $lessList;
  }

  function primes(){
    return false;
  }
}
