<?php

namespace App;

class Cart
{
  private $list;

  function __construct()
  {
    $this->list = array();
  }

  public function add($product)
  {
    $flag=0;
    if(!isset($this->list)) array_push($this->list,$product);
    
    foreach($this->list as $key => $value){
      if($value->id==$product->id){
          $this->list[$key]->qty+=1;
          $flag=1;
      }
    }
    if($flag==0) array_push($this->list,$product);
  }

  public function list()
  {
    print_r($this->list);
  }



  public function count($total=0){
    foreach($this->list as $value){
      $total +=$value->qty;
    }

    echo $total;
  }

  public function remove($id){
    foreach($this->list as $key => $value){
      if($value->id==$id){
          unset($this->list[$key]);
      }
    }
  }

  public function total($total=0){
    foreach($this->list as $key => $value){
      $total+=($value->price*$value->qty);
    }
    return $total;
  }

  public function clear(){
    $this->list = array();
  }

  public function increase($id){
    foreach($this->list as $key => $value){
      if($value->id==$id){
        $this->list[$key]->qty+=1;
      }
    }
  }

  public function decrease($id){
    foreach($this->list as $key => $value){
      if($value->id==$id){
        $this->list[$key]->qty-=1;
        if($this->list[$key]->qty==0) unset($this->list[$key]);
      }
    }
  }
}
