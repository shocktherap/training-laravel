<?php

namespace App;

class Item
{
  public $id, $name, $price, $qty;
  
  public function __construct($id, $name, $price, $qty=1)
  {
    $this->id = $id;
    $this->name = $name;
    $this->price = $price;
    $this->qty = $qty;
  }
}