<?php

namespace App;

class BasketBall
{
  public $home, $away;
  private $homeScore;
  private $awayScore;

  public function __construct($home, $away, int $homeScore = 0, int $awayScore = 0)
  {
    $this->home = $home;
    $this->away = $away;
    $this->homeScore = $homeScore;
    $this->awayScore = $awayScore;
  }

  function homeName(){
    return $this->home;
  }

  function awayName(){
    return $this->away;
  }

  function homeScore(){
    return $this->homeScore;
  }

  function homeThreePoints(){
    $this->homeScore += 3;
  }

  function homeOnePoint(){
    $this->homeScore += 1;
  }

  function homeTwoPoints(){
    $this->homeScore += 2;
  }

  function awayScore(){
    return $this->awayScore;
  }

  function awayThreePoints(){
    $this->awayScore += 3;
  }

  function awayTwoPoints(){
    $this->awayScore += 2;
  }

  function awayOnePoint(){
    $this->awayScore += 1;
  }
}
