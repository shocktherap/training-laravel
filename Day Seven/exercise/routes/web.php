<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Notification;

use App\Http\Controllers\UserController;
use App\Notifications\HelloNotification;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', [UserController::class, 'index']);

Route::get('/send', function () {
    $message = 'Hello World!';
    
    event(new App\Events\NewMessage($message));
    $user = User::find(1);
    // $user->notify(new HelloNotification());
});
