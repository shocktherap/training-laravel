<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
  public function index()
  {
    $users = User::paginate(15);
    $newUser  = User::find(1);

    return view('users.index', [
      'users' => $users,
      'newUser' => $newUser
    ]);
  }
}