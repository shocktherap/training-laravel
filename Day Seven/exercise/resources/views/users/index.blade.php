@extends('layout.master')
@section('content')

<div id="count">{{ $newUser->notifications->count() }}</div>
<div id="notifications"></div>

{{-- <section class="container mx-auto p-6 font-mono">
  <div class="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
    <div class="w-full overflow-x-auto">
      <table class="w-full">
        <thead>
          <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
            <th class="px-4 py-3">Name</th>
            <th class="px-4 py-3">Email</th>
            <th class="px-4 py-3">Aksi</th>
          </tr>
        </thead>
        <tbody class="bg-white">
          @foreach($users as $user)
            <tr class="text-gray-700">
              <td class="px-4 py-3 border">
                <div class="flex items-center text-sm">
                  <div class="relative w-8 h-8 mr-3 rounded-full md:block">
                    <img class="object-cover w-full h-full rounded-full" src="https://images.pexels.com/photos/5212324/pexels-photo-5212324.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260" alt="" loading="lazy" />
                    <div class="absolute inset-0 rounded-full shadow-inner" aria-hidden="true"></div>
                  </div>
                  <div>
                    <p class="font-semibold text-black">{{$user->name}}</p>
                  </div>
                </div>
              </td>
              <td class="px-4 py-3 text-ms font-semibold border">{{$user->email}}</td>
              <td class="px-4 py-3 text-sm border">6/4/2000</td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
  {{ $users->links() }} --}}
</section>
@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script>
const countEl = document.querySelector('#count');

Echo.channel('global')
    .listen('NewMessage', (e) => {
      console.log(e);

      const notif = document.querySelector('#notifications');

      notif.innerHtml = notif.innerHtml + e.message;
      countEl.innerHtml = parseInt(countEl.innerHtml);
    });
  
</script>