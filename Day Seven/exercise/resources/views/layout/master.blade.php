<!doctype html>
<html>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  @include('layout.header')
  <div class="container mx-auto">
    @yield('content')
  </div>
  @include('layout.footer')
  </div>
</body>
</html>