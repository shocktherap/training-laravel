<?php
  $sensored_word = ['saya', 'pasar'];
  $phrase = "Hari ini saya ingin pergi ke pasar";
  $split_phrase = explode(" ", $phrase);

  foreach ($split_phrase as $key => $value) {
    if (in_array($value, $sensored_word)) {
      // var_dump($key);
      $split_phrase[$key] = '***';
    }
  }
  echo implode(" ", $split_phrase);