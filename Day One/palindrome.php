<?php
  $word_listings = 
    ['Kasur ini rusak', 'Makan sate kambing', 'Ira hamil lima hari', 'Ibu Ratna antar ubi'];

  $random_array = array_rand($word_listings, 1);
  $word = $word_listings[$random_array];

  $low_case_word = strtolower($word);
  $word_no_space = str_replace(' ', '', $low_case_word);

  $reverse = strrev($word_no_space);

  print("$word : ");

  if($word_no_space == $reverse){
    print('true');
  } else {
    print('false');
  }