<?php
  $value = rand(0, 100);
  print("Value : $value" . PHP_EOL);
  print("Grade : ");

  switch ($value) {
    case $value == 100:
      print('A');
      break;
    case ($value >= 80):
      print('B');
      break;
    case ($value >= 70):
      print('C');
      break;
    case ($value >= 60):
      print('B');
      break;
    case ($value <= 59):
      print('E');
      break;
    default:
      print 'Value unreachable';
      break;
  }