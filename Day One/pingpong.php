<?php
  for ($number = 0; $number <= 100; $number++) {
    switch ($number) {
      case ($number % 3 == 0 && $number % 5 == 0):
        echo "Tuing". PHP_EOL;
        break;
      case ($number % 3 == 0):
        echo "Ping". PHP_EOL;
        break;
      case ($number % 5 == 0):
        echo "Pong". PHP_EOL;
        break;
      default:
        echo $number . PHP_EOL;
        break;
    }
  }