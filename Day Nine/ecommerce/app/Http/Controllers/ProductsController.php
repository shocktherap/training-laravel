<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductsController extends Controller
{
    public function show($id)
    {
        return view('products.show', [
          'product' => Product::find($id)
        ]);
    }
}