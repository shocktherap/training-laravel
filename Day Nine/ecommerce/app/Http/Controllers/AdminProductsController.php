<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class AdminProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $user = auth()->user();
      $products = $user->products()->paginate(8);

      return view('admin.products.index', [
        'products' => $products,
        'user' => $user
      ]);
    }

    public function create(){
      $user = auth()->user();
      return view('admin.products.create', [
        'user'=> $user
      ]);
    }

    public function store(Request $request)
    {
      $user = auth()->user();
      $validated = $request->validate([
        'name' => 'required',
        'sku' => 'required',
        'description' => 'required',
        'weight' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'sale_price' => 'required',
        'status' => 'required',
        'featured' => 'required',
        'image' => 'required|mimes:jpeg,png,bmp,tiff |max:4096',
        'thumbnail' => 'required|mimes:jpeg,png,bmp,tiff |max:4096',
      ], $messages = [
        'mimes' => 'Only jpeg, png, bmp,tiff are allowed.'
    ]);
      
      if($validated){
        $product = new Product();
        $uploadImageFile  = $request->image;
        $imageFileName    = $uploadImageFile->hashName();
        $uploadImageFile->storeAs('public/images', $imageFileName);

        $uploadThumbFile  = $request->thumbnail;
        $imageThumbName   = $uploadThumbFile->hashName();
        $uploadThumbFile->storeAs('public/images', $imageThumbName);
    
        $product->fill($request->all());
        $product->image     = $imageFileName;
        $product->thumbnail = $imageThumbName;
        $product->user_id   = $user->id;

        if($product->save()){
          return redirect('/admin/products')->with('status', 'Data berhasil disimpan');
        } else {
          
          return redirect('/admin/products')->with('status', 'Gagal simpan ke database');
        }	
      } else {
        return back()->withInput();
      }
      
    }

    public function edit($id){
      $user = auth()->user();
      $product = $user->products->find($id);
   
      return view('admin.products.edit', [
             'product' => $product,
             'user' => $user
      ]);
    }

    public function update(Request $request, $id)
    {
      $user = auth()->user();
      $validated = $request->validate([
        'name' => 'required',
        'sku' => 'required',
        'description' => 'required',
        'weight' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'sale_price' => 'required',
        'status' => 'required',
        'featured' => 'required',
        'image' => 'mimes:jpeg,png,bmp,tiff |max:4096',
        'thumbnail' => 'mimes:jpeg,png,bmp,tiff |max:4096',
      ], $messages = [
        'mimes' => 'Only jpeg, png, bmp,tiff are allowed.'
    ]);
      
      if($validated){
        $product = $user->products->find($id);
        
        if($request->image && $request->thumbnail){
          $destinationPath = 'storage/images';

          $uploadImageFile  = $request->image;
          $imageFileName    = $uploadImageFile->hashName();
          $uploadImageFile->move($destinationPath, $imageFileName);

          $uploadThumbFile  = $request->thumbnail;
          $imageThumbName   = $uploadThumbFile->hashName();
          $uploadThumbFile->move($destinationPath, $imageThumbName);

          $product->image     = $imageFileName;
          $product->thumbnail = $imageThumbName;
        }
        
        $product->update($request->all());

        if($product->save()){
          return redirect('/admin/products')->with('status', 'Data berhasil disimpan');
        } else {
          return redirect('/admin/products')->with('status', 'Gagal simpan ke database');
        }	
      } else {
        return back()->withInput();
      }
    }

    public function destroy($id){
      $user = auth()->user();
      $product = $user->products->find($id);

      if($product->delete()){
        $messages = 'data berhasil di hapus';
      } else {
        $messages = 'data gagal di hapus';
      }

      return back()->with('status', $messages);
    }
}
