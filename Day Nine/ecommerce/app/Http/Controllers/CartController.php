<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function add($id){
        $product = Product::find($id);

        if(!$product) {
            abort(404);
        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "name" => $product->name,
                        "quantity" => 1,
                        "price" => $product->price,
                    ]
            ];
            session()->put('cart', $cart);
						notify()->success($product->name . ' telah ditambahkan. Qty: '. $cart[$id]['quantity']);
            return redirect()->back();
        }

        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
            $counter = 0;
            foreach($cart as $item){
                $counter += $item['quantity'];
            }
            if($counter < 100){
                $cart[$id]['quantity']++;
                session()->put('cart', $cart);
                notify()->success($product->name . ' telah ditambahkan. Qty: '. $cart[$id]['quantity']);
            } else {
                notify()->warning($product->name . ' gagal ditambahkan. Kuota cart anda telah melebihi 100');
            }

            return redirect()->back();
        }

				// if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
					"name" => $product->name,
					"quantity" => 1,
					"price" => $product->price,
				];

				session()->put('cart', $cart);
				notify()->success($product->name . ' telah ditambahkan. Qty: '. $cart[$id]['quantity']);
				return redirect()->back();
    }
}
