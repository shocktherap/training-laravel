<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $user = auth()->user();

      return view('profile.index', [
        'user' => $user
      ]);
    }

    public function applyRole(Request $request){
      $user = auth()->user();

      $user->role_id = $request->role_id;
      $user->role_approved_at = null;

      $user->save();
      return back()->with('message','Property is updated .');;
    }
}
