@extends('layout.login-app')

@section('content')
<div class="bg-white lg:w-4/12 md:6/12 w-10/12 m-auto my-10 shadow-md">
    <div class="py-8 px-8 rounded-xl">
        <h1 class="font-medium text-2xl mt-3 text-center">Register</h1>
        <form method="POST" action="{{ route('register') }}" class="mt-6" >
            @csrf
            
            <div class="my-5 text-sm">
                <label for="name" class="block text-black">Name</label>
                <input type="text" name="name" value="{{ old('name') }}" autofocus id="name" class="rounded-sm px-4 py-3 mt-3 focus:outline-none bg-gray-100 w-full" placeholder="name" required autocomplete="name"/>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            
            <div class="my-5 text-sm">
                <label for="email" class="block text-black">Email</label>
                <input type="text" name="email" value="{{ old('email') }}" autofocus id="email" class="rounded-sm px-4 py-3 mt-3 focus:outline-none bg-gray-100 w-full" placeholder="email" required autocomplete="email"/>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="my-5 text-sm">
                <label for="password" class="block text-black">Password</label>
                <input type="password" name="password" id="password" class="rounded-sm px-4 py-3 mt-3 focus:outline-none bg-gray-100 w-full" placeholder="Password" required autocomplete="current-password" />
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="my-5 text-sm">
                <label for="password_confirmation" class="block text-black">Password Confirmation</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="rounded-sm px-4 py-3 mt-3 focus:outline-none bg-gray-100 w-full" placeholder="Password Confirmation" required autocomplete="current-password-confirmation" />
                @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <button type="submit" class="block text-center text-white bg-gray-800 p-3 duration-300 rounded-sm hover:bg-black w-full">{{ __('Register') }}</button>
        </form>
        <p class="mt-12 text-xs text-center font-light text-gray-400"> Have an account? <a href="/login" class="text-black font-medium"> Login </a>  </p> 
    </div>
</div>
@endsection
