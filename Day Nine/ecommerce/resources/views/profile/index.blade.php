@extends('layout.admin.master')

@section('content')

<!-- Profile tab -->
<!-- About Section -->
<div class="bg-white p-3 shadow-sm rounded-sm">
    <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
        <span clas="text-green-500">
            <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
            </svg>
        </span>
        <span class="tracking-wide">About</span>
    </div>
    <div class="text-gray-700">
        <div class="grid md:grid-cols-1 text-sm">
            <div class="grid grid-cols-3">
                <div class="px-4 py-2 font-semibold">Email</div>
                <div class="px-4 py-2">
                    <a class="text-blue-800" href="{{ $user->email }}">{{ $user->email }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="text-gray-700">
        <div class="grid md:grid-cols-1 text-sm">
            <div class="grid grid-cols-3">
                <div class="px-4 py-2 font-semibold">Name</div>
                <div class="px-4 py-2">
                    <a class="text-blue-800" href="mailto:jane@example.com">{{ $user->name }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of about section -->

<div class="my-4"></div>

<div class="bg-white p-3 shadow-sm rounded-sm">
    <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
        <span clas="text-green-500">
            <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
            </svg>
        </span>
        <span class="tracking-wide">Toko</span>
    </div>
    <div class="text-gray-700">
        <div class="grid md:grid-cols-1 text-sm">
            <div class="grid grid-cols-3">
                <div class="px-4 py-2 font-semibold">Status Toko</div>
                @if ($user->role_id == 2)
                <div class="px-4 py-2">
                    @if ($user->role_approved_at)
                    <div class="text-blue-800">Toko telah Dibuka</div>
                    @endif
                </div>
                <div class="px-4 py-2">
                    @if ($user->role_approved_at)
                    <form action="/profile/applyRole" method="post">
                        @csrf
                        <input type="hidden" name="role_id" value=3></input>
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Tutup Toko
                        </button>
                    </form>
                    @else
                    Menunggu Pembukaan Toko
                    @endif
                </div>
                @else
                <div class="px-4 py-2">
                    @if ($user->role_approved_at)
                    <div class="text-blue-800">Anda Belum Mempunyai Toko</div>
                    @endif
                </div>
                <div class="px-4 py-2">
                    @if ($user->role_approved_at)
                    <form action="/profile/applyRole" method="post">
                        @csrf
                        <input type="hidden" name="role_id" value=2></input>
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Buka Toko
                        </button>
                    </form>
                    @else
                    Menunggu Penutupan Toko
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
