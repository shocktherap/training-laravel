<!doctype html>
<html>
<head>
  <title>ecommerce</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div>
    @include('layout.header')
  </div>
  <main>
    <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
      @include('notify::messages')
      <x:notify-messages />
      @yield('content')
    </div>
  </main>
  @include('layout.footer')
  <script src="{{ asset('js/app.js') }}"></script>
  @notifyJs
</body>
</html>