<!doctype html>
<html>

<head>
    <title>ecommerce</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        :root {
            --main-color: #4a76a8;
        }

        .bg-main-color {
            background-color: var(--main-color);
        }

        .text-main-color {
            color: var(--main-color);
        }

        .border-main-color {
            border-color: var(--main-color);
        }

    </style>
</head>

<body>
    <div>
        @include('layout.header')
    </div>
    <main>
        <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
            <div class="bg-gray-100">
                <div class="container mx-auto my-5 p-5">
                    <div class="md:flex no-wrap md:-mx-2 ">
                        <!-- Left Side -->
                        <div class="w-full md:w-3/12 md:mx-2">
                            <!-- Profile Card -->
                            <div class="bg-white p-3 border-t-4 border-green-400">
                                <div class="image overflow-hidden">
                                    <img class="h-auto w-full mx-auto" src="https://lorempixel.com/800/600/cats/"
                                        alt="">
                                </div>
                                <h1 class="text-gray-900 font-bold text-xl leading-8 my-1">{{ $user->name }}</h1>
                                <h3 class="text-gray-600 font-lg text-semibold leading-6">Owner at Hime/Her Company Inc.
                                </h3>
                                <p class="text-sm text-gray-500 hover:text-gray-600 leading-6">Lorem ipsum dolor sit
                                    amet
                                    consectetur adipisicing elit.
                                    Reprehenderit, eligendi dolorum sequi illum qui unde aspernatur non deserunt</p>
                                <ul
                                    class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                                    <li class="flex items-center py-3">
                                        <span>Status</span>
                                        @if ($user->email_verified_at)
                                        <span class="ml-auto"><span
                                                class="bg-green-500 py-1 px-2 rounded text-white text-sm">Verified</span></span>
                                        @else
                                        <span class="ml-auto"><span
                                                class="bg-red-500 py-1 px-2 rounded text-white text-sm">Not
                                                Verified</span></span>
                                        @endif

                                    </li>
                                    <li class="flex items-center py-3">
                                        <span>Products</span>
                                        <span class="ml-auto">{{ $user->created_at->format('Y M d') }}</span>
                                    </li>
                                    <li class="flex items-center py-3">
                                        <span>Role</span>
                                        <span class="ml-auto">{{ $user->role->name; }}</span>
                                    </li>
                                </ul>
                                @if ($user->role_id == 2)
                                <ul
                                    class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                                    <li class="flex items-center py-3 text-gray-900">
                                        <span>Manage Toko</span>
                                    </li>
                                    <li><a href="/admin/products"
                                            class="flex items-center py-3 hover:text-gray-900 transition-colors duration-200 {{ (request()->is('admin/products*')) ? 'bg-white' : ''  }}">
                                            <div
                                                class="mr-3 rounded-md bg-gradient-to-br from-light-blue-400 to-indigo-500">
                                                <svg class="h-6 w-6" viewBox="0 0 24 24">
                                                    <path d="M17 13a1 1 0 011 1v3a1 1 0 01-1 1H8.5a2.5 2.5 0 010-5H17z"
                                                        fill="#93C5FD"></path>
                                                    <path
                                                        d="M12.743 7.722a1 1 0 011.414 0l2.122 2.121a1 1 0 010 1.414l-6.01 6.01a2.5 2.5 0 11-3.536-3.536l6.01-6.01z"
                                                        fill="#BFDBFE"></path>
                                                    <path d="M6 7a1 1 0 011-1h3a1 1 0 011 1v8.5a2.5 2.5 0 01-5 0V7z"
                                                        fill="#EFF6FF">
                                                    </path>
                                                    <path d="M9.5 15.5a1 1 0 11-2 0 1 1 0 012 0z" fill="#60A5FA"></path>
                                                </svg></div>Products
                                        </a></li>
                                    <li><a href="/admin/carts"
                                            class="flex items-center py-3 hover:text-gray-900 transition-colors duration-200">
                                            <div class="mr-3 rounded-md bg-gradient-to-br from-pink-500 to-rose-500">
                                                <svg class="h-6 w-6" viewBox="0 0 24 24">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M9 6C10.0929 6 11.1175 6.29218 12 6.80269V16.8027C11.1175 16.2922 10.0929 16 9 16C7.90714 16 6.88252 16.2922 6 16.8027V6.80269C6.88252 6.29218 7.90714 6 9 6Z"
                                                        fill="#FFF1F2"></path>
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M15 6C16.0929 6 17.1175 6.29218 18 6.80269V16.8027C17.1175 16.2922 16.0929 16 15 16C13.9071 16 12.8825 16.2922 12 16.8027V6.80269C12.8825 6.29218 13.9071 6 15 6Z"
                                                        fill="#FECDD3"></path>
                                                </svg></div>Pesanan
                                        </a></li>
                                    <a href="/admin/invoices"
                                        class="flex items-center py-3 hover:text-gray-900 transition-colors duration-200">
                                        <div class="mr-3 rounded-md bg-gradient-to-br from-yellow-400 to-orange-500">
                                            <svg class="h-6 w-6" viewBox="0 0 24 24">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M13.196 6.02a1 1 0 01.785 1.176l-2 10a1 1 0 01-1.961-.392l2-10a1 1 0 011.176-.784z"
                                                    fill="#FDE68A"></path>
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M15.293 9.293a1 1 0 011.414 0l2 2a1 1 0 010 1.414l-2 2a1 1 0 01-1.414-1.414L16.586 12l-1.293-1.293a1 1 0 010-1.414zM8.707 9.293a1 1 0 010 1.414L7.414 12l1.293 1.293a1 1 0 11-1.414 1.414l-2-2a1 1 0 010-1.414l2-2a1 1 0 011.414 0z"
                                                    fill="#FDF4FF"></path>
                                            </svg></div>Invoice
                                    </a>
                                </ul>
                                @endif

                            </div>
                            <!-- End of profile card -->
                            <div class="my-4"></div>
                        </div>
                        <!-- Right Side -->
                        <div class="w-full md:w-9/12 mx-2 h-64">
                          @yield('content')
                        </div>
                    </div>
                    <!-- End of profile tab -->
                </div>
            </div>
        </div>
        </div>
        </div>
    </main>
    @include('layout.footer')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
</body>

</html>
