<!doctype html>
<html>
<head>
  <title>ecommerce</title>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <main>
    <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
      @yield('content')
    </div>
  </main>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>