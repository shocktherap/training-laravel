<form action="/carts/{{ $product->id }}" method="POST" class="rounded-md flex items-center justify-center text-white font-bold">  
@csrf
<button type="submit" class="flex ml-auto text-white bg-purple-500 border-0 py-2 px-6 focus:outline-none hover:bg-purple-600 rounded">Add to <svg class="h-5 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="-1 -2 24 24" stroke="white">
    <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
    </svg></button>
</form>
