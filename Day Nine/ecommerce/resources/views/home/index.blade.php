@extends('layout.master')
@section('content')
<header class="bg-white shadow">
  <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
    <h1 class="text-3xl font-bold text-gray-900">
      Best Seller
    </h1>
  </div>
</header>

<div class="bg-white">
  <div class="max-w-2xl mx-auto py-16 px-4 sm:py-8 sm:px-6 lg:max-w-7xl lg:px-8">
    <h2 class="sr-only">Products</h2>

    <div class="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
      @foreach ($products as $product)
        <div class="group">
        <a href="/products/{{ $product->id }}" class="">
          <div class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
          @if ($product->image)
            <img src="{{asset('storage/images/' .$product->image)}}" class="w-full h-48 object-center object-cover group-hover:opacity-75" alt="Responsive image">
          @else
            <img src="http://lorempixel.com/800/600/cats/" alt="Tall slender porcelain bottle with natural clay textured body and cork stopper." class="w-full h-full object-center object-cover group-hover:opacity-75">
          @endif
            
          </div>
          <h3 class="mt-4 text-sm text-gray-700">
            {{ $product->name }}
          </h3>
        </a>
        <div class="grid grid-cols-2 grid-rows-1 grid-flow-col gap-4 py-2">
          <p class="mt-1 text-lg font-medium text-gray-900">
            ${{ $product->price }}
          </p>
          <x-add-to-cart :product=$product></x-add-to-cart>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  {{ $products->links() }}
</div>
@endsection


