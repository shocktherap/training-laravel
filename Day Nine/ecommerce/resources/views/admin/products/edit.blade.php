@extends('layout.admin.master')

@section('content')
<div class="bg-white p-3 shadow-sm rounded-sm">
    <form class="w-full" action="/admin/products/{{ $product->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="flex flex-wrap -mx-1 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                    Name
                </label>
                <input name="name"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('name') ? 'border-red-500' : '' }} rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                    id="grid-first-name" type="text" placeholder="Jane" value="{{ old('name') ?? $product->name }}">
                @if ($errors->has('name'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('name') }}</p>
                @endif
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                    SKU
                </label>
                <input name="sku"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('sku') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-last-name" type="text" placeholder="Doe" value="{{ old('sku') ?? $product->sku }}">
                @if ($errors->has('sku'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('sku') }}</p>
                @endif
            </div>
        </div>
        <div class="flex flex-wrap -mx-1 mb-6">
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                    Description
                </label>
                <textarea name="description"
                    class="resize border rounded-md appearance-none block w-full text-gray-700 border {{ $errors->has('description') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">{{ old('description') ?? $product->description }}</textarea>
                <p class="text-gray-600 text-xs italic">Make it as long and as crazy as you'd like</p>
                @if ($errors->has('description'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('description') }}</p>
                @endif
            </div>
        </div>
        <div class="flex flex-wrap -mx-1 mb-6">
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                    Quantity
                </label>
                <input name="quantity"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('quantity') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-city" type="number" placeholder="Quantity with numeric value" value="{{ old('quantity') ?? $product->quantity }}" min=0>
                @if ($errors->has('quantity'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('quantity') }}</p>
                @endif
            </div>
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                    Price
                </label>
                <input name="price"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('price') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-zip" type="number" placeholder="Price with numeric value" value="{{ old('price') ?? $product->price }}" min=0>
                @if ($errors->has('price'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('price') }}</p>
                @endif
            </div>
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                    Sale Price
                </label>
                <input name="sale_price"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('sale_price') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-zip" type="number" placeholder="Sale Price with numeric value" value="{{ old('sale_price') ?? $product->sale_price }}" min=0>
                @if ($errors->has('sale_price'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('sale_price') }}</p>
                @endif
            </div>
        </div>
        <div class="flex flex-wrap -mx-1 mb-6">
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                    Weight
                </label>
                <input name="weight"
                    class="appearance-none block w-full text-gray-700 border {{ $errors->has('weight') ? 'border-red-500' : '' }} border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-city" type="number" placeholder="Weight in Kilo" value="{{ old('weight') ?? $product->weight }}" min=0>
                @if ($errors->has('weight'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('weight') }}</p>
                @endif
            </div>
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                    Status
                </label>
                <div class="relative">
                    <select name="status"
                        class="block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="grid-state">
                        <option value=1>Active</option>
                        <option value=0>Inactive</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                    </div>
                </div>
            </div>
            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                    Feature
                </label>
                <div class="relative">
                    <select name="featured"
                        class="block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="grid-state">
                        <option value=1>Active</option>
                        <option value=0>Inactive</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex flex-wrap -mx-1 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                    Product Image
                </label>
                <input name="image" type='file' class="{{ $errors->has('image') ? 'border-red-500' : '' }}" value="{{ old('image') ?? $product->image }}" accept="image/png, image/gif, image/jpeg"/>
                @if ($product->image)
                    <div class="bg-rose-300">
                        <img class="object-contain h-48" src="{{asset('storage/images/' .$product->image)}}">
                    </div>
                    <a href="{{ asset('storage/images/' .$product->image) }}" download>Download</a>
                @endif

                @if ($errors->has('image'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('image') }}</p>
                @endif
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                    Product Thumbnail
                </label>
                <input name="thumbnail" type='file' class="{{ $errors->has('thumbnail') ? 'border-red-500' : '' }}" value="{{ old('thumbnail') ?? $product->thumbnail }}" accept="image/png, image/gif, image/jpeg"/>
                @if ($product->image)
                    <div class="bg-rose-300">
                        <img class="object-contain h-48" src="{{asset('storage/images/' .$product->thumbnail)}}">
                    </div>
                    <a href="{{ asset('storage/images/' .$product->thumbnail) }}" download>Download</a>
                @endif

                @if ($errors->has('thumbnail'))
                <p class="text-red-500 text-xs italic">{{ $errors->first('thumbnail') }}</p>
                @endif
            </div>
        </div>
        <button
            class="ml-2 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700"
            type="submit">
            Simpan
        </button>
    </form>
</div>
@endsection
