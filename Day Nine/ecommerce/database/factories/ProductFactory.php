<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $price = $this->faker->numberBetween($min = 10, $max = 1000);
        $name = $this->faker->city();

        return [
            // 'image' => $this->faker->image('public/storage/images',640,480, 'cats', false),
            'thumbnail' => '',
            'sku' => $name. '_' .$this->faker->cityPrefix(),
            'name' => $name,
            'slug' => $this->faker->slug(),
            'description' => $this->faker->text(400),
            'quantity' => $this->faker->numberBetween($min = 10, $max = 1000),
            'weight' => $this->faker->numberBetween($min = 100, $max = 1000),
            'price' => $price,
            'sale_price' => $price - ($price*0.1),
            'status' => $this->faker->boolean(),
            'featured' => $this->faker->boolean(),
            'user_id' => User::all()->random()->id,
        ];
    }
}
