<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Product::factory(100)->create();
        \App\Models\Role::factory()->create(['name'=> 'administrator']);
        \App\Models\Role::factory()->create(['name'=> 'seller']);
        \App\Models\Role::factory()->create(['name'=> 'buyer']);
    }
}
