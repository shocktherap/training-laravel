<form method="post" action="/login">
  @csrf

  <input type="email" name="email" placeholder="Email" />
  <input type="password" name="password" placeholder="Password"/>
  <label>
    <input type="checkbox" name="remember" /> Remember me
  </label>

  <button>Login</button>
</div>
