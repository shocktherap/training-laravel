<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::All();
        return $contacts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$rules = [
				'first_name' 				=> 'required',
				'last_name' 				=> 'required',
				'handphone_number' 	=> 'required',
				'phone_number' 			=> 'required',
				'email' 						=> 'required|email',
				'description' 			=> '',
			];
			
			$messages = [];

			$validation = Validator()->make(request()->all(), $rules, $messages);
			if ($validation->fails()) {
				return $validation->messages();
				
			} else {
				$contact = Contact::create($request->all());
        return $contact;
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        if($contact){
					return $contact;
				} else {
					return [
						'message' => 'Data Not Found'
					];
				}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
      $rules = [
				'first_name' 				=> 'required',
				'last_name' 				=> 'required',
				'handphone_number' 	=> 'required',
				'phone_number' 			=> 'required',
				'email' 						=> 'required|email',
				'description' 			=> '',
			];
			
			$messages = [];

			$validation = Validator()->make(request()->all(), $rules, $messages);
			if ($validation->fails()) {
				return $validation->messages();
				
			} else {
				$contact->update($request->all());
        return $contact;
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
      $contact->delete();

			return [
				'message' => 'Data deleted'
			];
    }
}
