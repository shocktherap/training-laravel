<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// Artisan::command('inspire', function () {
//     $this->comment(Inspiring::quote());
// })->purpose('Display an inspiring quote');

Artisan::command('Hello', function () {
    // $this->comment(Inspiring::quote());
    // $message = Inspiring::quote();

    // $this->line($message);
    // $this->info($message);
    // $this->comment($message);
    // $this->question($message);
    // $this->error($message);

    // $name = $this->ask('Nama Anda?');
    // $this->line('nama anda adalah: '. $name);

    $confirmed = $this->confirm('Oke?', true);
    var_dump($confirmed);

})->purpose('Display an amazing stuff');
