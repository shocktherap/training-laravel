<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;

class HelloWorldCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "check";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $name = $this->argument('name');
        // $uppercase = $this->option('uppercase');
        
        // if($uppercase){
        //     $this->info(strtoupper('Hello, ' .$name));
        // } else {
        //     $this->info('Hello, ' .$name);
        // }
        
        // $age = $this->argument('age');
        // $this->info($name);
        // $this->info($age);

        // $this->notify('title', 'body');

        // $start = now();
        // sleep(5);
        // $end = now();

        // if($start->diffInSeconds($end) > 3){
        //     $this->notify('Selesai', 'Your Ticket is ready to Download!');
        // }

        // \Storage::append('./contoh.txt', now());
        // \Log::info('testing');

        $process = new Process(['ls', '-ll']);
        $process->run();

        $this->line($process);
    }
}
