<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use Spatie\Browsershot\Browsershot;
use Symfony\Component\Process\Process;

class Screenshot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'screenshot {url} {--output=screenshot.png} {--format=png}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   $fileName = $this->setFileName();
				$url = $this->argument('url');
        Browsershot::url($url)->save($this->folderPath() .$fileName);
				
				$this->line($fileName);
    }

		public function folderPath()
		{
			return './public/screenshots/';
		}

		function findFileOnFolder($output){
			$process = new Process(['find', $this->folderPath(), '-name', $output]);
			$process->run();
			return $process->getOutput();
		}

		function setFileName(){
			$output = $this->option('output');
			$findFileOnFolder = $this->findFileOnFolder($output);
			$i = 0;

			$fileName = '';

			if(!$findFileOnFolder) {return $fileName = $output;};
			
			$splitFileName 	= explode('.', $output);
			$newFileName 		= $splitFileName[0];
			$newFileFormat 	= $this->setFileFormat(end($splitFileName));

			while (true) {
				$i += 1;
				$number = str_pad($i, 3, '0', STR_PAD_LEFT);
				$tempFileName = $newFileName .'-' .$number .'.' .$newFileFormat;
				$findFileOnFolder = $this->findFileOnFolder($tempFileName);
				if (!$findFileOnFolder){
					$fileName = $tempFileName;
					break;
				}
			}

			return $fileName;
		}

		function setFileFormat($fileFormat){
			$finalFormat = '';
			$format = $this->option('format');

			$finalFormat = ($format) ? $format : $fileFormat;
			return $finalFormat;
		}

}
