<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Ip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check the Private IP Address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
			$process = new Process(['internal-ip', '--ipv4']);
    	$process->run();
			$this->line($process->getOutput());
    }

}
