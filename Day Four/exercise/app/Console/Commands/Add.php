<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Add extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add {numbers*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sum all off argument';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = 0;
        foreach($this->argument('numbers') as $number){
            $count += $number;
        }
        $this->info($count);
    }
}
