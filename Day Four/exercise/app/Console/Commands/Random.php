<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Random extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Random {--length=32} {--numbers=true} {--letters=true} {--lowercase} {--uppercase}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check the Obfuscate text';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
			$characters 	= $this->setAlphanumeric();
			$randomString = $this->generateRandomString($characters);
			$finalString  = $this->setSize($randomString);

			$this->line($finalString);
    }

		public function generateRandomString($characters, $randomString = ''){
			for ($i = 0; $i < $this->option('length'); $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}

			return $randomString;
		}

		public function setSize($characters){
			if ($this->option('uppercase')) $characters = strtoupper($characters);
			if ($this->option('lowercase')) $characters = strtolower($characters);

			return $characters;
		}

		public function setAlphanumeric($characters = ''){
			if($this->option('numbers') == 'true' && $this->option('letters') == 'true'){
				$characters .= $this->numbers(); $characters .= $this->letters;
			} elseif($this->option('numbers') == 'true'){
				$characters .= $this->numbers();
			} elseif($this->option('letters') == 'true'){
				$characters .= $this->letters();
			}

			return $characters;
		}

		public function numbers()
		{
			return '0123456789';
		}

		function letters(){
			return 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
}
