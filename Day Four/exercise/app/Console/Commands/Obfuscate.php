<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Obfuscate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'obfuscate {text}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check the Obfuscate text';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
			$obfuscate = '';
			$text = $this->argument('text');
      foreach(unpack('C*', $text) as $letter){
				$obfuscate .= $this->letterDecode($letter);
			}
			
			$this->line($obfuscate);
    }

		public function letterDecode($letter){
			return '&#' .$letter .';';
		}
}
