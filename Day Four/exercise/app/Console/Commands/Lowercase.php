<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Lowercase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lowercase {sentence}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make sentence to lowercase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(strtolower($this->argument('sentence')));
        
    }
}
