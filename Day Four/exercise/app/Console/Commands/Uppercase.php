<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Uppercase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uppercase {sentence}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make sentence to uppercase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(strtoupper($this->argument('sentence')));
        
    }
}
