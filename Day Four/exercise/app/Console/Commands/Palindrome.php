<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Palindrome extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'palindrome {text}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check the palindrome text';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
			$text = $this->argument('text');
      $this->line('String: ' .$text);

			$low_case_word = strtolower($text);
			$word_no_space = str_replace(' ', '', $low_case_word);
			$reverse = strrev($word_no_space);

			$valid = ($word_no_space == $reverse) ? 'Yes' : 'No';
			$this->line('Is palindrome? ' .$valid);
    }
}
