<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel;

class Convert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converting CSV/XLX/XLSX';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      Excel::load('./report2018.xlsx', function($file) {
			})->convert('report2018.csv');
    }
}
