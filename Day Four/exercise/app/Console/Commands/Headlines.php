<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use voku\helper\HtmlDomParser;

class Headlines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'headlines';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check the headline of oneline newspaper';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
			$html = HtmlDomParser::file_get_html('https://www.kompas.com/');
    	$elements = $html->find('.most__link');
			// var_dump($elements);
			foreach($elements as $element) {
    		$href = $element->findOne('a')->getAttribute('href');
    		$text = $element->findOne('.most__title')->text();
				// var_dump($text);
    		$this->line('Title: ' .$text);
				$this->line('URL: ' .$href);
				$this->line('');
			}
    }

}
