<?php

namespace Database\Factories;

use App\Models\Blog;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filePath = storage_path('public/images');

        return [
                'title' => $this->faker->sentence(5),
                'content' => $this->faker->text(2000),
                'main_image' => $this->faker->image('public/storage/images',640,480, 'cat', false),
        ];
    }
}
