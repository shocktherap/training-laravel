@extends('layout.master')

@section('content')
  <div class="bg-light p-5 rounded">
    <h1>Users</h1>
  </div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody start="{{ $users->firstItem() }}">
      @foreach($users as $user)
        <tr>
          <td>{{$user->name}}</td>
          <td>{{$user->email}}</td>
          <td>Aksi</td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $users->links() }}

@endsection
