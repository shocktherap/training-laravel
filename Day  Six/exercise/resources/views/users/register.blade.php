@extends('layout.login-master')

@section('content')
  <div class="py-5 text-center">
    <h2>Registrasi</h2>
    <p class="lead">Welcome to Blogs Applications</p>
  </div>
	@if (session('status'))
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{ session('status') }}
    </div>    
  @endif
	<form method="post" action="/register" class="form-signin">
	@csrf
	<h1 class="h3 mb-3 font-weight-normal">Please fill the blank</h1>
  <label for="inputName" class="sr-only">Nama</label>
  <input type="text" name="name" id="inputName" class="form-control" placeholder="Nama" required	 autofocus>

  <label for="inputEmail" class="sr-only">Alamat Email</label>
  <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>

  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
  <button class="btn btn-primary btn-block" type="submit">Daftar</button>
  </br>
	Sudah punya akun? <a href="{{ url('/login') }}">Login</a>
</form>
@endsection
