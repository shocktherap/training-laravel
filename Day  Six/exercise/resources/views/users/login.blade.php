@extends('layout.login-master')

@section('content')
<div class="py-5 text-center">
  <h2>Login</h2>
  <p class="lead">Welcome to Blogs Applications</p>
</div>
@if (session('status'))
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{ session('status') }}
    </div>    
  @endif
<form method="post" action="/login" class="form-signin">
  @csrf
  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
  <label for="inputEmail" class="sr-only">Email address</label>
  <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" value="remember-me"> Remember me
    </label>
  </div>
  <button class="btn btn-primary btn-block" type="submit">Sign in</button>
  </br>
	Belum punya akun? <a href="{{ url('/register') }}">Daftar</a>
</form>
@endsection