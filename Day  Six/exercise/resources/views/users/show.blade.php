@extends('layout.master')

@section('content')
  <table class=table table-condensed>
  <thead>
    <tr>
      <th>#</th>
      <th>Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Nama</td>
      <td>{{ $user->name; }}</td>
    </tr>
    <tr>
      <td>email</td>
      <td>{{ $user->email; }}</td>
    </tr>
  </tbody>
  </table>

@endsection

