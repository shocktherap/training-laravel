@extends('layout.master')

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">

      <div start="{{ $blogs->firstItem() }}" class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        @foreach($blogs as $blog)
          <div class="col">
            <div class="card shadow-sm">
              <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>

              <div class="card-body">
                <h5>{{ $blog->title }}</h5>
                <p class="card-text">{{ substr($blog->content, 0, 140) }}...</p>
                <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                    <a href="{{ url('/blogs/' . $blog->id) }}" class="btn btn-sm btn-outline-secondary">View</a>
                  </div>
                  <small class="text-muted">{{$blog->created_at}}</small>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <hr></hr>
      {{ $blogs->links() }}
    </div>
  </div>
  

@endsection

