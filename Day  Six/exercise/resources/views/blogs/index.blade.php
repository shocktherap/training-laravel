@extends('layout.master')

@section('content')
  @if (session('status'))
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{ session('status') }}
    </div>    
  @endif
  
  <div class="bg-light p-5 rounded">
    <h1>Blogs</h1>
  </div>
  <div class="btn-group">
    <a href="{{ url('/blogs/new') }}" class="btn btn-md btn-outline-primary">Buat Baru</a>
  </div>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Judul</th>
        <th scope="col">Konten</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    
    <tbody start="{{ $blogs->firstItem() }}">
      @foreach($blogs as $blog)
        <tr>
          <td>
            <a href="{{ url('/blogs/' . $blog->id) }}">
              {{$blog->title}}
              </a>
          </td>
          <td>
          {{substr($blog->content, 0, 500)}}...
          </br>
          <small class="text-muted">{{$blog->created_at}}</small>
          </td>
          <td>
            <div class="btn-group" role="group" aria-label="Basic example">
              <a href="{{ url('/blogs/' .$blog->id . '/edit') }}" class="btn btn-secondary">Ubah</a>
              <form action="/blogs/{{$blog->id}}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class='btn btn-danger' data-confirm='Are you sure you want to delete?'>Hapus</button>
              </form>
            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $blogs->links() }}

@endsection

