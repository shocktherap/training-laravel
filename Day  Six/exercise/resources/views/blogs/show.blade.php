@extends('layout.master')

@section('content')
  <div class="bg-light p-5 rounded">
    <h1>{{ $blog->title }}</h1>
    
    @if ($blog->main_image)
      <img src="{{asset('storage/images/' .$blog->main_image)}}" class="img-fluid" alt="Responsive image">
    @else
      <img src="http://lorempixel.com/400/200/cat/" />
    @endif
    
    <p><small class="text-muted">{{$blog->created_at}}</small></p>
    <p class="lead" style="text-align: justify;">{{ $blog->content }}</p>
  </div>

  @if (session('status'))
    <div class="alert alert-primary" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{ session('status') }}
    </div>    
  @endif

  <form method="post" action="/blogs/{{$blog->id}}/comments/new" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="exampleInput2">Komentar</label>
      <textarea name='content' class="form-control" id="exampleFormControlTextarea1" rows="2">{{ old('content') }}</textarea>
      {{ $errors->first('content') }}
    </div>
    <hr></hr>
    <button type="submit" class="btn btn-primary">Kirim</button>
  </form>

  <div class="list-group">
    @foreach($comments as $comment)
    <a href="#" class="list-group-item list-group-item-action d-flex gap-3 py-3" aria-current="true">
      {{-- <img src="https://github.com/twbs.png" alt="twbs" width="32" height="32" class="rounded-circle flex-shrink-0"> --}}
      <div class="d-flex gap-2 w-100 justify-content-between">
        <div>
          <h6 class="mb-0">{{$comment->user->name}}</h6>
          <p class="mb-0 opacity-75">{{$comment->content}}</p>
          <form action="/blogs/{{$blog->id}}/comments/{{$comment->id}}" method="POST" class="form form-horizontal">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class='btn btn-danger btn-sm' data-confirm='Are you sure you want to delete?'>Hapus</button>
          </form>
        </div>
        <small class="opacity-50 text-nowrap">{{$comment->created_at}}</small>
      </div>
    </a>
    @endforeach
  </div>
  <hr></hr>
  {{ $comments->links() }}
  
@endsection

