@extends('layout.master')

@section('content')
  <div class="bg-light p-5 rounded">
    <h1>Edit Blog</h1>
  </div>
  @foreach($errors->all() as $error)
    <div class="alert alert-warning" role="alert">
      {{ $error }}
    </div>
  @endforeach

  <form method="post" action="/blogs/{{$blog->id}}/edit" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="exampleInput1">Judul</label>
      <input type="text" name='title' class="form-control" id="exampleInput1" aria-describedby="titleHelp" placeholder="Masukan Judul" value={{old('title', $blog->title)}}>
      {{ $errors->first('title') }}
    </div>
    <div class="form-group">
      <label for="exampleInput2">Konten</label>
      <textarea name='content' class="form-control" id="exampleFormControlTextarea1" rows="10">{{old('content', $blog->content)}}</textarea>
      {{ $errors->first('content') }}
    </div>
    <div class="form-group">
      <label for="exampleInput3">Main Image</label>
      <input type="file" name="main_image" class='form-control-file' id="exampleInput3">
      <div class='row'>
        <div class='col-md-1'>
          <img src="{{asset('storage/images/' .$blog->main_image)}}" class="img-thumbnail" alt="Responsive image">
          <a href="{{ asset('storage/images/' .$blog->main_image) }}" download>Download</a>
        </div>
      </div>
      
      
      {{ $errors->first('main_image') }}
    </div>
    
    <hr></hr>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

