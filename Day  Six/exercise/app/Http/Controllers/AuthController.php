<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    function getRegister(){
			return view('users/register');
		}

		function postRegister(){
			
			$user = new User();
      $user->name = request()->name;
      $user->email = request()->email;
      $user->password = bcrypt(request()->password);

			if($user->save()){
				return redirect('/login')->with('status', 'Registrasi User Berhasil');
			} else {
				return back()->withInput();
			}	
		}

		function getLogin(){
			return view('users/login');
		}

		function postLogin(){
			$credentials = request()->only('email', 'password');

			if (Auth::attempt($credentials)) {
					return redirect()->intended('/admin');
			}

			return back()->with('status', 'Gagal login');

		}

		function postLogout(){
			Auth::logout();

    	return redirect('/login');
		}

}
