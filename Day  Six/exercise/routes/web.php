<?php

use Illuminate\Support\Facades\Route;
use App\Models\User as User;
use App\Models\Blog as Blog;
use App\Models\Comment as Comment;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$blogs = Blog::Paginate(5);

	return view('home/index', [
		'blogs' => $blogs,
	]);
});

Route::get('/admin', [AdminController::class, 'index'])
  ->middleware('auth');

Route::get('/register', [AuthController::class, 'getRegister']);

Route::post('/register', [AuthController::class, 'postRegister']);

Route::get('/login', [AuthController::class, 'getLogin'])->name('login');

Route::post('/login', [AuthController::class, 'postLogin']);

Route::get('/logout', [AuthController::class, 'postLogout']);


Route::get('/users', function() {
	$users = User::Paginate(15);

	return view('users/index', [
		'users' => $users,
	]);
});

Route::get('/users/{user}', function(User $user) {
	return view('users/show', [
		'user' => $user,
	]);
});

Route::get('/blogs/new', function() {
	return view('blogs/new');
});

Route::post('/blogs/new', function() {
	$blog = new Blog();
	
	$validated = request()->validate([
    'title' => 'required',
    'content' => 'required',
		'main_image' => 'required',
  ]);

	if($validated){
		$uploadFile = request()->main_image;
		$file_name = $uploadFile->hashName();

		$path = $uploadFile->storeAs('public/images', $file_name);

		$blog->title 			= request()->title;
		$blog->content 		= request()->content;
		$blog->main_image = $file_name;
	}

	if($blog->save()){
		return redirect('/blogs')->with('status', 'Data berhasil disimpan');
	} else {
		return back()->withInput();
	}	
});

Route::delete('/blogs/{blog}', function(Blog $blog) {

	if($blog->delete()){
		return redirect('/blogs')->with('status', 'Data berhasil dihapus');
	} else {
		return back()->with('status', 'Data gagal dihapus');
	}	
});

Route::get('/blogs/{blog}/edit', function(Blog $blog) {
	return view('blogs/edit', [
		'blog' => $blog,
	]);
});

Route::post('/blogs/{blog}/edit', function(Blog $blog) {	
	$validated = request()->validate([
    'title' => 'required',
    'content' => 'required',
		'main_image' => 'mimes:jpeg,jpg,png,gif'
  ]);
	
	if($validated){
		$blog->title 			= request()->title;
		$blog->content 		= request()->content;

		if (request()->main_image && request()->main_image->isValid()){ 	
			$uploadFile = request()->main_image;
			
			$file_name 	= $uploadFile->hashName();	
			$path 			= $uploadFile->storeAs('public/images', $file_name);

			$blog->main_image = $file_name;
		}
	}

	if($blog->save()){
		return redirect('/blogs')->with('status', 'Data berhasil dirubah');
	} else {
		return back()->withInput();
	}

	return view('blogs/edit', [
		'blog' => $blog,
	]);
});

Route::post('/blogs/{blog}/comments/new', function(Blog $blog) {
	$comment = new Comment();

	$validated = request()->validate([
    'content' => 'required',
  ]);	

	if($validated){
		$comment->blog_id			= $blog->id;
		$comment->content 		= request()->content;
		$comment->user_id 		= User::all()->random()->id;
	}

	if($comment->save()){
		return redirect('/blogs/' .$blog->id)->with('status', 'Data berhasil disimpan');
	} else {
		return back()->withInput();
	}
});

Route::delete('/blogs/{blog}/comments/{comment}', function(Blog $blog, Comment $comment) {
	if($comment->delete()){
		return redirect('/blogs/' .$blog->id)->with('status', 'Data berhasil dihapus');
	} else {
		return back()->withInput();
	}
});

Route::get('/blogs/{blog}', function(Blog $blog) {
	$comments = Comment::where('blog_id', $blog->id)
											->orderBy('created_at', 'desc')
											->paginate(15);

	return view('blogs/show', [
		'blog' => $blog,
		'comments' => $comments
	]);
});

Route::get('/blogs', function() {
	$blogs = Blog::orderBy('created_at', 'DESC')->paginate(15);

	return view('blogs/index', [
		'blogs' => $blogs,
	]);
})->middleware('auth');
