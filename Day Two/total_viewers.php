<?php
$selected_films = 0;
$films = json_decode(retrieveUserFile(), true);

foreach($films as $key => $value) {
    $selected_films += $value['viewers'];
}

echo $selected_films;


function retrieveUserFile(){
  $file = 'films.json';
  return file_get_contents("./$file");
}