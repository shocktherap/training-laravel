<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Day Two!</title>
  </head>
  <body class="bg-light">
    <div class="container">
      <div class="py-5 text-center">
        <h2>Registration Form</h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleInputName1">Nama</label>
              <input name="name" class="form-control" id="exampleInputName1" placeholder="Enter nama">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputPhoneNumber1">Nomor Telepon</label>
              <input name="phoneNumber" class="form-control" id="exampleInputPhoneNumber1" placeholder="Enter Nomor Telepon">
            </div>
            <div class="form-group">
              <label for="exampleFormControlFile1">Profile Picture</label>
              <input name="profilePict" type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
            <div class="form-group">
              <label for="exampleFormControlFile2">Dokumen</label>
              <input name="document" type="file" class="form-control-file" id="exampleFormControlFile2">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>


<?php
  $required = array('name', 'email', 'phoneNumber', 'profilePict', 'document');

  // Loop over field names, make sure each one exists and is not empty
  $error = false;
  foreach($required as $field) {
    if (empty($_POST[$field])) {
      $error = true;
    }
  }
  
  if ($error) {
    echo "All fields are required.";
  } else {
    saveToCSV($_POST, $_FILES);
    saveFileToFolder($_FILES);
    echo "success";
  }

  function saveToCSV($params, $files){
    $values = array(
      $params['name'], 
      $params['email'], 
      $params['phoneNumber'],
      $files['profilePict']['name'],
      $files['document']['name'],
    );

    $file = 'users.csv';
    $file = fopen("./form_registrasi/$file", 'a');

    fputcsv($file, $values);
    fclose($file);
  }

  function saveFileToFolder($params) {
    move_uploaded_file(
      $params['profilePict']['tmp_name'], 
      './form_registrasi/' . time() . $params['profilePict']['name']
    );

    move_uploaded_file(
      $params['document']['tmp_name'], 
      './form_registrasi/' . time() . $params['document']['name'] 
    );
  }
  