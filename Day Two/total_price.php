<?php
$items = json_decode(retrieveUserFile(), true);
$total = 0;

foreach($items as $key => $value) {
  $total += ($value['price'] * $value['qty']);
}
echo $total;

function retrieveUserFile(){
  $file = 'total_price.json';
  return file_get_contents("./$file");
}