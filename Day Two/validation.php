<?php
function validate($username){
  $uppercase = preg_match('/^[a-zA-Z0-9-._]+$/', $username);

  if(!$uppercase || !(strlen($username) <= 20) || !(strlen($username) > 3) ) {
    echo 'Invalid Username';
  } else {
    echo 'Valid Username';
  }
}

$username = 'Awesome1';
validate($username);