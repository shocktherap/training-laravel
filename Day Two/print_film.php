<?php
$selected_films = [];
$films = json_decode(retrieveUserFile(), true);

foreach($films as $key => $value) {
  if($value['viewers'] > 500){
    $selected_films[]= $value['title'];
  }
}

foreach($selected_films as $selected_film){
  echo $selected_film . PHP_EOL;
}


function retrieveUserFile(){
  $file = 'films.json';
  return file_get_contents("./$file");
}