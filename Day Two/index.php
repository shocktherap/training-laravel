<?php
  session_start();
  echo date('yyyy-mm-dd H:i:s');

  setcookie('nama', 'ahmad hizqil');
  $profile = [
    'name' => 'Ahmad Hizqil',
    'nip' => '199107312015031006'
  ];
  setcookie('data', json_encode($profile));

  $sum = function ($a, $b) {
    return $a + $b;
  };

  echo $sum(4,2);
  /**
   * @param array <string> $emails
   * @param callable $callback
   * @return void
   */

  function sendMail(array $emails, callable $callback = null){
    foreach($emails as $email){
      echo 'Email telah dikirim ke ' . $email . PHP_EOL;
    }

    if ($callback) $callback();
  };

  $emails = ['rudi', 'budi', 'yudi'];

  sendMail($emails, function(){
    echo 'mail success';
  });


  