<?php
$start = new DateTimeImmutable();
$end = new DateTimeImmutable('2021-07-31');

function counter($start, $end){
  $diff = $start->diff($end);
  echo $diff->format('%R%a days');
};

counter($start, $end);
